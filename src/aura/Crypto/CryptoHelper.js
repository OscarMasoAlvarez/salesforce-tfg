({
	initHelper : function(component, event, helper) {
		this.getObjectData(component, event, helper);
	},
	getObjectData : function(component, event, helper) {
			console.log('Util')
			var action = component.get("c.getHeroe");
			console.log('Util RecordID: ' + component.get("v.recordId"))
			action.setParams({ "heroeid" : component.get("v.recordId")});
			action.setCallback(this, function(response) {	
				var data = response.getReturnValue();
				console.log('DATOS: ' + JSON.stringify(data));
				component.set('v.heroe', data);
				component.set('v.datoContra', data.Password__c);
				console.log('DATOS data: ' + data.Password__c);
				var state = response.getState();
                    console.log('Respuesta: ' + state);
				if (state == "SUCCESS") {
					component.set('v.loaded', "true"); 
					this.showToast( 'Password Cargada' ,'SUCCESS', 5, 'SUCCESS', 'dismissible');                
				}
			});   
		$A.enqueueAction(action);
	},
	showData : function(component, event, helper,password) {
		var datoContra = component.get("v.datoContra");
		console.log('Contraseña Guardada: ' + datoContra);
		console.log('Contraseña Puesta: ' + password);

		if(datoContra == null){
			this.showToast('No has puesto contraseña', 'ERROR',5, 'ERROR', 'dismissible');  
		}
		else if(datoContra != password){
			this.showToast('La contraseña es incorrecta','ERROR',5, 'ERROR', 'dismissible');  
		}
		else if(datoContra == password){
			var action = component.get("c.seeHeroeName");

			action.setParams({ "heroeid" : component.get("v.recordId")});
			action.setCallback(this, function(response) {	
				var data = response.getReturnValue();
				console.log('DATOS: ' + JSON.stringify(data));
				component.set('v.heroe', data);
				component.set('v.Realname', data.Real_Name__c);
				console.log('DATOS data: ' + data.Real_Name__c);
				console.log('Respuesta: ' + state);
				var state = response.getState();
				if (state == "SUCCESS") {
					component.set('v.loaded', "false"); 
					component.set('v.NameReal', "true"); 
					this.showToast('Contraseña correcta','SUCCESS',5, 'SUCCESS', 'dismissible');                  
				}
			});
			$A.enqueueAction(action); 
		}
	},
	showToast : function(title,type,duration,message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
			"type" : type,
			"duration" : duration,
            "message": message,
            "mode": mode
        });
        toastEvent.fire();
    }
})