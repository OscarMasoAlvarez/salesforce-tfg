({
	handleInit : function(component, event, helper) {
		console.log('inicio');
		helper.initHelper(component, event, helper);	
	},
	showSpinner: function(component, event, helper) {
		 component.set("v.Spinner", true); 
	},
	hideSpinner : function(component,event,helper){
		component.set("v.Spinner", false);
	},
	clickCancel : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
    },
	clickAccept : function(component, event, helper) {
		var password = component.find("Password").get("v.value");;
		helper.showData(component, event, helper, password);
	}
})