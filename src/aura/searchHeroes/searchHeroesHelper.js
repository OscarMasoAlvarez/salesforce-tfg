({
	initHelper : function(component, event, helper) {
		this.getObjectData(component, event, helper);
	},
	getObjectData : function(component, event, helper) {
		console.log('Datos');
		var action = component.get("c.getData");
	
			action.setCallback(this, function(response) {
				var state = response.getState();
				var data = response.getReturnValue();
					if (state === "SUCCESS") {
						component.set('v.wrapperList', JSON.parse(data));
						component.set('v.wrapperListAux', JSON.parse(data));
						console.log('Datos: ' + data);
						console.log('Datos: ', data);

						console.log('Wrapper: ' , component.get('v.wrapperList'));
					}
				});
			$A.enqueueAction(action);
	},
	FilterGetData: function (component, event, helper) {
        var listaUtil = component.get('v.wrapperList');

        var filtro =    component.find('enter-search').get('v.value') + ',' +  
						component.find('Compania').get('v.value') + ',' +
						component.find('v.FilterMenu').get('v.value');
						
        // Filtro de 5 Columnas SIEMPRE
        console.log('Filtro: ' + filtro);

        this.FilterData(component, event, helper, filtro, listaUtil);
    },
	FilterData: function (component, event, helper, filtro, listaUtil){

		var list =  JSON.stringify(listaUtil);
		var filtro = filtro.split(',');
		console.log(filtro);

		var action = component.get('c.filterDataUse');


		action.setParams({ "filter" : filtro , "obje" : list});
	
			action.setCallback(this, function(response) {
				var state = response.getState();
			
				if (state === "SUCCESS") {
					 component.set('v.wrapperListAux', JSON.parse(response.getReturnValue()));
					 console.log('Wrapper: ' , component.get('v.wrapperListAux'));
				}
			});
		$A.enqueueAction(action);
    }
})