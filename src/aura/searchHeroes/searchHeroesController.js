({
	handleInit : function(component, event, helper) {
		console.log('inicio');
		helper.initHelper(component, event, helper);
	},
	OrderOption: function (component, event,helper) {
        // This will contain the index (position) of the selected lightning:menuItem
        var SelectMenu = event.getParam("value");
        console.log('Ordenar: ' + SelectMenu);
        component.set('v.FilterMenu', SelectMenu);

        // Find all menu items
        var menuItems = component.find("menuItems");
        menuItems.forEach(function (menuItem) {
            // For each menu item, if it was checked, un-check it. This ensures that only one
            // menu item is checked at a time

            if (menuItem.get("v.checked")) {
                console.log('Opcion 1');
                menuItem.set("v.checked", false);
            }
            // Check the selected menu item
            if (menuItem.get("v.value") === SelectMenu) {
                console.log('Opcion 2');
                menuItem.set("v.checked", true);
            }
            console.log('Datos: ' + menuItem.get("v.value")  + ' Datos 2 : ' + menuItem.get("v.checked") + ' Datos 3: ' + SelectMenu);
        });
        
        helper.FilterGetData(component, event, helper);
    },
    openHeroe: function (component, event,helper) {
		var selectedItem = event.currentTarget;
        var id = selectedItem.dataset.id;

        console.log('id necesaria: ' + id);
        console.log('id necesaria: ' + selectedItem);

        var url = "https://salestfg30dosa-dev-ed.lightning.force.com/lightning/r/Heroe__c/" + id + "/view";

        window.open(url);
    },
    UpdateColor:  function (component, event,helper) {
        var selectedItem = event.currentTarget;
        $A.util.addClass(selectedItem,'orangeColor');
        $A.util.removeClass(selectedItem,'clearColor');
    },
    DeleteColor:  function (component, event,helper) {
        var selectedItem = event.currentTarget;
        $A.util.addClass(selectedItem,'clearColor');
    },
    FilterGlobal: function (component, event,helper) {
        helper.FilterGetData(component, event, helper);
    },
    mostrarMas: function (component, event,helper) {
        var cont = component.get('v.cont');
        cont +=10;
        component.set('v.cont',cont);
    },
    mostrarTodas: function (component, event,helper) {
        var lista = component.get('v.wrapperListAux');
        component.set('v.cont',lista.length);
    },
    restablecer: function (component, event,helper) {

        var filtro1 = component.find('v.enter-search').get('v.value');
		var filtro2 = component.find('v.Compania').get('v.value');
		var filtro6 = component.find('v.FilterMenu').get('v.value');

        console.log('AQUI: ' + filtro1 + filtro2);

        filtro1 = "";
        filtro2 = "v.Sin Filtro";
        filtro6 = "v.Sin Filtro";
        
        component.set('v.enter-search',filtro1);
		component.set('v.Compania',filtro2);
		component.set('v.FilterMenu',filtro6);

        var cont = component.get('v.cont');
        cont =10;
        component.set('v.cont',cont);

         helper.FilterGetData(component, event, helper);
        
    }
})