({
    initHelper : function(component, event, helper) {
        helper.getLocation(component, event, helper);
    },
    getLocation : function(component, event, helper) {
        var longitud;
        var latitud;
        var startPos;
        var geoSuccess = function(position) {
            startPos = position;
            longitud = position.coords.longitude;
            latitud = position.coords.latitude;
            console.log('Longitud:' + longitud)
            console.log('Latitude:' + latitud)
            component.set('v.longitud', longitud);
            component.set('v.latitud', latitud);
            var markers = [
                {
                    location: {
                        'Latitude': position.coords.latitude,
                        'Longitude': position.coords.longitude
                    },
                    
                    title: 'Localización Actual',
                    description: ''
                }
            ];
            
            helper.putLocations(component, event, helper, markers);
            
            component.set('v.zoomLevel', 10);
            
            
        };
        navigator.geolocation.getCurrentPosition(geoSuccess);
    },
    
    putLocations : function(component, event, helper, markers) {
        var action = component.get("c.putLoca");
        console.log(markers) 
        action.setParams({
            'Longitude': markers[0].location.Longitude,
            'Latitude': markers[0].location.Latitude
        });
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            //component.set('v.wrapperList', JSON.parse(data));
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var heroes = JSON.parse(data);
                for(var i=0; i<=heroes.length -1; i++){
                    console.log(heroes[i])
                    markers.push(heroes[i]);
                }
                console.log(heroes)
                console.log(markers);
                component.set('v.mapMarkers',markers);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "SUCCESS",
                    "type":"SUCCESS",
                    "message": "Datos ingresados en el mapa",
                    "duration": "3000"
                });
                toastEvent.fire();
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);       
    },
    
    showToast : function(type, message, title, duration, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message,
            "duration": duration,
            "mode": mode
        });
        toastEvent.fire();
    }
})