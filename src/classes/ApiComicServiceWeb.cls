@RestResource(urlMapping='/ComicApi/*')
global class ApiComicServiceWeb {

    @HttpPost
    global static void upsertComic(){
        System.debug('JSON Entrada:' + RestContext.request.requestBody.toString());

        ApiComicServiceWebModel req = (ApiComicServiceWebModel)JSON.deserialize(RestContext.request.requestBody.toString(), ApiComicServiceWebModel.class);
        ApiComicServiceWebController.creaComic(req);
    }
}