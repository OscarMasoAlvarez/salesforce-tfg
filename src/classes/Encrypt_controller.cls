public class Encrypt_controller {
    @AuraEnabled
    public static Heroe__c getHeroe(String heroeid) {
        system.debug('STRING: ' + heroeid);
        List<Heroe__c> heroe = [SELECT Id, Real_Name__c,CryptoKey2__c,Password__c FROM Heroe__c WHERE Id = :heroeid];
        system.debug('[CheckInController] getHeroe : heroe   '+ heroe);
        return heroe[0];
    }
    @AuraEnabled
    public static Heroe__c seeHeroeName(String heroeid){
        List<Heroe__c> LstHeroe = new List<Heroe__c>();
        List<Heroe__c> heroe = [SELECT Id, Real_Name__c,CryptoKey2__c,Password__c FROM Heroe__c WHERE Id = :heroeid];
        
        system.debug('Desencriptamos --> ');
        try{
            for(Heroe__c heroes : heroe){
                Blob keys = EncodingUtil.base64Decode(heroes.CryptoKey2__c);
                Blob Dencrypt =  EncodingUtil.base64Decode(heroes.Real_Name__c);
                Blob encrypted = Crypto.decryptWithManagedIV('AES128', keys, Dencrypt);
                heroes.Real_Name__c = EncodingUtil.base64Encode(encrypted);
                
                Lstheroe.add(heroes);
            }
            
            return LstHeroe[0];
        }
        catch(Exception ex){
            system.debug('Exception: ' + ex);
        }
        return null;
    }
}