@isTest
public class Encrypt_controller_TEST {
    
    @testSetup
    static void setup(){
        Comic__c comic = testFactory.generateComic();
        Heroe__c heroe = testFactory.generateHeroe();
    }
    
    @isTest
    public static void testHeroeComponent(){

        Heroe__c heroe = [SELECT Real_Name__c,Password__c, Secret__c FROM Heroe__c];

        heroe.Secret__c = true;
        update heroe;

        Test.startTest();

        Heroe__c heroeV1 =  Encrypt_controller.getHeroe(heroe.id);

        system.assertequals(heroe.Password__c, 'Contraseña');

        Heroe__c heroeV2 = Encrypt_controller.seeHeroeName(heroe.id);

        Test.stopTest();
    }

    @isTest
    public static void testHeroeComponentV1(){

        Heroe__c heroe = [SELECT Real_Name__c,Password__c, Secret__c FROM Heroe__c];

                
        try{

            Test.startTest();

                Heroe__c heroeV2 = Encrypt_controller.seeHeroeName('Error');

            Test.stopTest();
        }
        catch(Exception ex){

            system.assertequals(ex.getMessage().contains('Argument cannot be null.'),true);
        }
    }
}