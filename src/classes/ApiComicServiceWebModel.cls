public class ApiComicServiceWebModel{
	public Integer code;	//200
	public String status;	//Ok
	public String copyright;	//© 2019 MARVEL
	public String attributionText;	//Data provided by Marvel. © 2019 MARVEL
	public String attributionHTML;	//<a href="http://marvel.com">Data provided by Marvel. © 2019 MARVEL</a>
	public String etag;	//e29688036aca41f6dfcffa3089c652050f4f2be1
	public cls_data data;
	public class cls_data {
		public Integer offset;	//0
		public Integer total;	//45938
		public Integer count;	//20
		public cls_results[] results;
	}
	public class cls_results {
		public Integer id;	//183
		public Integer digitalId;	//0
		public String title;	//Startling Stories: The Incorrigible Hulk (2004) #1  Comic name	
		public Integer issueNumber;	//1
		public String variantDescription;	//
		public String description;	
		public String modified;	//-0001-11-30T00:00:00-0500
		public String isbn;	//
		public String upc;	//5960605429-00811
		public String diamondCode;	//
		public String ean;	//
		public String issn;	//
		public String format;	//Comic // Type!
		public Integer pageCount;	//0
		public cls_textObjects[] textObjects;
		public String resourceURI;	//http://gateway.marvel.com/v1/public/comics/183
		public cls_urls[] urls;
		public cls_series series;
		public cls_prices[] prices;
		public cls_thumbnail thumbnail;
		public cls_creators creators;
		public cls_characters characters;
		public cls_stories stories;
		public cls_events events;
	}
	public class cls_textObjects {
		public String type;	//issue_solicit_text
		public String language;	//en-us
		public String text;	// Description	
	}
	public class cls_urls {
		public String type;	//detail
		public String url;	// Pagina Web HTML
	}
	public class cls_series {
		public String resourceURI;	//http://gateway.marvel.com/v1/public/series/565
		public String name;	//Startling Stories: The Incorrigible Hulk (2004)
	}
	public class cls_dates {
		public String type;	//onsaleDate
	}
	public class cls_prices {
		public String type;	//printPrice
		public Double price;	// precio
	}
	public class cls_thumbnail {
		public String path;	//http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available
		public String extension;	//jpg
	}
	public class cls_creators {
		public Integer available;	//1
		public String collectionURI;	//http://gateway.marvel.com/v1/public/comics/183/creators
		public cls_items[] items;
		public Integer returned;	//1
	}
	public class cls_characters {
		public Integer available;	//1
		public String collectionURI;	//http://gateway.marvel.com/v1/public/comics/183/characters
		public cls_items[] items;
		public Integer returned;	//1
	}
	public class cls_stories {
		public Integer available;	//2
		public String collectionURI;	//http://gateway.marvel.com/v1/public/comics/183/stories
		public cls_items[] items;
		public Integer returned;	//2
	}
	public class cls_events {
		public Integer available;	//0
		public String collectionURI;	//http://gateway.marvel.com/v1/public/comics/183/events
		public cls_items[] items;
		public Integer returned;	//0
	}
	public	class cls_items {
		public String resourceURI;	//http://gateway.marvel.com/v1/public/stories/1891
		public String name;	//Cover #1891
		public String type;	//cover
	}
}