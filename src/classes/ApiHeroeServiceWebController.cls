global class ApiHeroeServiceWebController {
    
    public static void creaHeroe(ApiHeroeServiceWebModel rest){

        Heroe__c heroe = new Heroe__c();
        if(rest.name != null){
            List<Heroe__c> heroeV1 =[ SELECT id 
                                FROM Heroe__c 
                                WHERE Name= :rest.id LIMIT 1];
            if(!heroeV1.isEmpty()){
                system.debug('Entramos');
                heroe.id = heroeV1[0].id;
            }
            else{
                heroe.ExtID__c = rest.id;
            }
        }
        heroe.Name = rest.name;
        if(rest.biography.full_name != null){
            heroe.Real_Name__c = rest.biography.full_name;
            heroe.Secret__c = true;
        }
        else{
             heroe.Secret__c = false;
        }
        heroe.ImagenHero__c = rest.image.url;
        heroe.Password__c = rest.id;


        upsert heroe;

            if(rest.response == 'success'){
                if(rest.powerstats != null){
                    Powerstats__c pow = new Powerstats__c();
                        pow.Heroe__c = heroe.id;
                        pow.Intelligence__c = integer.valueof(rest.powerstats.intelligence);
                        pow.strength__c = integer.valueof(rest.powerstats.strength);
                        pow.speed__c = integer.valueof(rest.powerstats.speed);
                        pow.durability__c = integer.valueof(rest.powerstats.durability);
                        pow.power__c = integer.valueof(rest.powerstats.power);
                        pow.combat__c = integer.valueof(rest.powerstats.combat);

                        upsert pow;
                }
                if(rest.biography != null){
                    Biography__c biogr = new Biography__c();
                        biogr.Heroe__c= heroe.id;
                        biogr.full_name__c = rest.biography.full_name;
                        biogr.alter_egos__c = rest.biography.alter_egos;
                        biogr.aliases__c = String.join(rest.biography.aliases, ' ');
                        biogr.place_of_birth__c = rest.biography.place_of_birth;
                        biogr.first_appearance__c = rest.biography.first_appearance;
                        biogr.publisher__c = rest.biography.publisher;
                        biogr.alignment__c = rest.biography.alignment;

                        upsert biogr;
                }
                if(rest.appearance != null){
                    Appearance__c Appear = new Appearance__c();
                        Appear.Heroe__c = heroe.id;
                        Appear.gender__c = rest.appearance.gender;
                        Appear.race__c =  rest.appearance.race;
                        Appear.height__c = String.join(rest.appearance.height, ', ');
                        Appear.weight__c =  String.join(rest.appearance.weight, ', ');
                        Appear.eye_color__c = rest.appearance.eye_color;
                        Appear.hair_color__c = rest.appearance.hair_color;

                        upsert Appear;
                }
                if(rest.work != null){
                    Work__c work = new Work__c();
                        work.Heroe__c = heroe.id;
                        work.occupation__c = rest.work.base;
                        work.base__c = rest.work.occupation;

                        upsert work;
                }
                if(rest.connections != null){
                    Connections__c connec = new Connections__c();
                        connec.Heroe__c = heroe.id;
                        connec.group_affiliation__c = rest.connections.group_affiliation;
                        connec.relatives__c = rest.connections.relatives;

                        upsert connec;
                }
            }
    }
}