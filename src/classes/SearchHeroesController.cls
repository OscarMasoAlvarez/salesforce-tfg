public class SearchHeroesController {
    @AuraEnabled
    public static string getData(){ 
        
        List<Heroes> LstComp = getHeroes();
        
        for(Heroes hero : LstComp){
            system.debug('Hero: ' + hero);
        }
        
        return JSON.serialize(LstComp);
    }
    // // @AuraEnabled
    // // public static string filterDataUse(string[] filter, String obje){
    
    // //     List<Heroes> lst_objs = (List<Heroes>) System.JSON.deserialize(obje, List<Heroes>.class);
    // //     List<Heroes> listNueva = new list<Heroes>();
    
    // //     listNueva = lst_objs;
    
    // //     List<Heroes> mapdate = new List<Heroes>();
    // //     List<Heroes> listaDeValor = new List<Heroes>();
    // //     List<Heroes> ListaDeVisual = new List<Heroes>();
    // //     map<String,Heroes> mapAZ = new map<String,Heroes>();
    // //     map<String,Heroes> mapZA = new map<String,Heroes>();
    // //     List<String> lstAZ = new List<String>();
    // //     List<String> lstZA = new List<String>();
    
    // //     List<Heroes> lstIterate = new List<Heroes>(); 
    
    
    // //     for(integer i = 0; i<filter.size(); i++){
    // //             switch on i {
    // //                 when  0 {
    // //                     if(filter[i] != 'Sin Filtro' && filter[i] != 'Todos'){
    // //                         for(integer  j= listNueva.size()-1; j >= 0; j--){
    // //                             for(integer x = 0; x<listNueva[j].Compania.size(); x++){
    // //                                 if(filter[i] != listNueva[j].Compania[x]){
    // //                                     listNueva.remove(j);
    // //                                     break;
    // //                                 }
    // //                             }
    // //                         }
    // //                     }
    // //                 }
    // //                 when 1 {
    // //                         for(integer  o = listNueva.size()-1; o >= 0; o--){
    // //                             if(!listNueva[o].Title.containsIgnoreCase(filter[i])){
    // //                                 listNueva.remove(o);
    // //                             }
    // //                         }  
    // //                 }
    // //                 when 2 {
    // //                         switch on filter[i]{
    // //                             when 'AaZ'{
    // //                                 // A la Z
    // //                                 for(Heroes hero : listNueva){
    // //                                     system.debug('Seguimiento: ' + hero);
    // //                                     mapAZ.put(hero.Title,hero);
    // //                                     lstAZ.add(hero.Title);
    // //                                 }
    // //                                 system.debug('Desordenada: ' + lstAZ);
    // //                                 lstAZ.sort();
    // //                                 system.debug('Ordenada: ' + lstAZ);
    // //                                 listNueva.clear();
    // //                                 for(String objemap : lstAZ){
    // //                                     listNueva.add(mapAZ.get(objemap));
    // //                                 }
    // //                             }
    // //                             when 'ZaA'{
    // //                                 // Z a la A
    // //                                  for(Heroes hero : listNueva){
    // //                                     system.debug('Seguimiento: ' + hero);
    // //                                     mapZA.put(hero.id,Heroes);
    // //                                     lstZA.add(hero.id);
    // //                                 }
    // //                                 system.debug('Desordenada: ' + lstZA);
    // //                                 lstZA.sort();
    // //                                 system.debug('Ordenada: ' + lstZA);
    // //                                 listNueva.clear();
    // //                                 lstIterate.clear();
    // //                                 for(String objemap : lstZA){
    // //                                     lstIterate.add(mapZA.get(objemap));
    // //                                 }
    // //                                  for(integer  p = lstIterate.size()-1; p >= 0; p--){
    // //                                         listNueva.add(lstIterate[p]);
    // //                                 } 
    // //                             }
    // //                             when 'undefined'{
    
    // //                             }
    // //                         }
    // //                 }
    // //                     System.debug('Que mandamos: ' + listNueva);
    // //                     return JSON.serialize(listNueva);
    // //         }
    // //     }
    
    // //     // return JSON.serialize(lst_objs);
    // //     system.debug('Hola');  
    // //     return null;
    // // }
    public  static List<Heroes> getHeroes(){
        // Clase creada para la union de todos los datos en un Dato asequible para lanzar a el programa
        List<Heroes> lstHeros = new List<Heroes>();
        
        map<id,Hero> mapHeroes = getHero();
        map<id,PowerStats> mapPowerStats = getPowerStats();
        system.debug('Errores: ' + getPowerStats());
        map<id,Appearance> mapAppearance = getAppearance();   
        system.debug('Errores: ' + getAppearance());
        
        for(Hero hero : mapHeroes.values()){
            system.debug('HeroPrueba: ' + hero);
            Heroes heros = new Heroes(
                hero.id,
                hero,
                mapPowerStats.get(hero.id),
                mapAppearance.get(hero.id)
            );
            system.debug('Lst: ' + mapPowerStats.get(hero.id));
            system.debug('Lst: ' + mapAppearance.get(hero.id));
            lstHeros.add(heros);
        }
        return lstHeros;
    }
    public static map<id,Hero> getHero(){
        
        List<Heroe__c> lstHero= new List<Heroe__c>([
            SELECT id,Name
            FROM Heroe__c]);
        Map<id,Hero> mapHeroAdd = new map<id,Hero>();
        
        
        
        for(Heroe__c heroe : lstHero){
            system.debug('HeroePrueba: ' + heroe.id);
            Hero newHero = new Hero(
                heroe.id,
                heroe.Name
            );
            
            mapHeroAdd.put(newHero.id,newHero);
        }
        if(mapHeroAdd != null){
            return mapHeroAdd;
        }
        return null;
    } 
    public static map<id,PowerStats> getPowerStats(){
        
        List<PowerStats__c> lstPowerStats= new List<PowerStats__c>([
            SELECT Heroe__c, Combat__c, Durability__c, Intelligence__c, Power__c, Speed__c, Strength__c
            FROM PowerStats__c
        ]);
        Map<id,PowerStats> mapPowerStatsAdd = new Map<id,PowerStats>();
        
        
        for(PowerStats__c Abiliti: lstPowerStats){
            PowerStats newPowerStats = new PowerStats(
                Abiliti.Heroe__c,
                Abiliti.Combat__c,
                Abiliti.Durability__c,
                Abiliti.Intelligence__c,
                Abiliti.Power__c,
                Abiliti.Speed__c,
                Abiliti.Strength__c
            );
            mapPowerStatsAdd.put(newPowerStats.heroeId,newPowerStats);
        }
        if(mapPowerStatsAdd != null){
            return mapPowerStatsAdd;
        }
        return null;
    }
    public static map<id,Appearance> getAppearance(){
        
        List<Appearance__c> lstAppearance= new List<Appearance__c>([
            SELECT Heroe__c,Gender__c,Height__c,Weight__c
            FROM Appearance__c
        ]);
        Map<id,Appearance> mapAppearanceAdd = new Map<id,Appearance>();
        
        for(Appearance__c Appearance : lstAppearance){
            Appearance newAppearance = new Appearance(
                Appearance.Heroe__c,
                Appearance.Gender__c,
                Appearance.Weight__c,
                Appearance.Height__c
            );
            mapAppearanceAdd.put(newAppearance.heroeId,newAppearance);
        }
        if(mapAppearanceAdd != null){
            return mapAppearanceAdd;
        }
        return null;
    }
    public class Heroes{
        public string id;
        public Hero Hero;
        public PowerStats PowerStats;
        public Appearance Appearance;
        
        public Heroes(string id, Hero Hero,PowerStats PowerStats,Appearance Appearance){
            this.id = id;
            this.Hero = Hero;
            this.PowerStats = PowerStats;
            this.Appearance = Appearance;           
        }
    }
    public class Hero{      
        public string id;
        public string HeroeName;

        
        public Hero(string id, string HeroeName){
            this.id = id;
            this.HeroeName = HeroeName;
        }
    }
    public class PowerStats{
        public string heroeId;
        public decimal Combat;
        public decimal Durability;
        public decimal Intelligence;
        public decimal Power;
        public decimal Speed;
        public decimal Strength;
        
        public PowerStats(string heroeId, decimal Combat,decimal Durability,decimal Intelligence,decimal Power,decimal Speed,decimal Strength){
            this.heroeId = heroeId;
            this.Combat = Combat;
            this.Durability = Durability;
            this.Intelligence = Intelligence;
            this.Power = Power;
            this.Speed = Speed;
            this.Strength = Strength;
        }
    }
    public class Appearance{
        public string heroeId;
        public string Gender;
        public string Height;
        public string Weight;
        
        public Appearance(string heroeId, string Gender, string Height, string Weight){
            
            this.heroeId = heroeId;
            this.Gender = Gender;
            this.Height = Height;
            this.Weight = Weight;
        }
    }
}