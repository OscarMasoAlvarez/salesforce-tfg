public class ApiHeroeServiceWebModel{
		public biography biography{get;set;}
		public work work{get;set;}
		public appearance appearance{get;set;}
		public powerstats powerstats{get;set;}
		public connections connections{get;set;}
		public String name{get;set;}
		public String id{get;set;}
		public image image{get;set;}
		public String response{get;set;}
	public class work{
		public String base{get;set;}
		public String occupation{get;set;}
	}
	public class powerstats{
		public String power{get;set;}
		public String speed{get;set;}
		public String durability{get;set;}
		public String strength{get;set;}
		public String combat{get;set;}
		public String intelligence{get;set;}
	}
	public class image{
		public String url{get;set;}
	}
	public class connections{
		public String group_affiliation{get;set;}
		public String relatives{get;set;}
	}
	public class biography{
		public String alignment{get;set;}
		public String place_of_birth{get;set;}
		public list<String> aliases{get;set;}
		public String first_appearance{get;set;}
		public String alter_egos{get;set;}
		public String publisher{get;set;}
		public String full_name{get;set;}
	}
	public class appearance{
		public String gender{get;set;}
		public list<String> weight{get;set;}
		public String eye_color{get;set;}
		public list<String> height{get;set;}
		public String hair_color{get;set;}
		public String race{get;set;}
	}
}