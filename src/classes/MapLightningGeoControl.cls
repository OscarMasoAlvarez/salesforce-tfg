public class MapLightningGeoControl {

    @AuraEnabled
    public static string putLoca(String Longitude, String Latitude){
        List<Heroe__c> lstHeroes = new List<Heroe__c>([
            SELECT id,Name
            FROM Heroe__c
        ]);
        Map<string,Biography__c> mapHeroesAPi = new Map<string,Biography__c>();
        
        Map<id,Biography__c> mapHeroes = new Map<id,Biography__c>([
            SELECT id,First_Appearance__c,Heroe__c
            FROM Biography__c
        ]);

        for(Biography__c biog : mapHeroes.values()){
            mapHeroesAPi.put(biog.Heroe__c,biog);
        }

        List<Postions> lstPosi = new List<Postions>();
        Boolean checkSumRes = false;
        for(Heroe__c Heroe : lstHeroes){
            Decimal randomNumber = (Math.random() * 0.1150);
            Decimal longitud = Decimal.valueOf(longitude);
            Decimal latitud = Decimal.valueOf(latitude);
            if(!checkSumRes){
                longitud -= randomNumber;
                latitud -= randomNumber;
            }else{
				longitud += randomNumber;
                latitud += randomNumber;               
            }
            Postions posi = new Postions(
                new Location(String.valueOf(longitud), String.valueOf(latitud)),
                'standard:lead',
                Heroe.Name,
                mapHeroesAPi.get(Heroe.id).First_Appearance__c
            );

            lstPosi.add(posi);
            if(!checkSumRes) checkSumRes = true;
            else checkSumRes = false;
        }
		System.debug(lstPosi);
        return JSON.serialize(lstPosi);
    }
    
    @AuraEnabled
    public static string getLoca(){
        List<Heroe__c> lstHeroes = new List<Heroe__c>([
            SELECT id,Name
            FROM Heroe__c
        ]);

        return JSON.serialize(lstHeroes);
    }
	
    public class Location{
        public string Longitude;
        public string Latitude;
        
        public Location(string Longitude, string Latitude){
            this.Longitude =Longitude;
            this.Latitude =Latitude;
        }
	}
    
    public class Postions{
        public Location location;
        public string icon;
        public string title;
        public string description;
        
        public Postions(Location location,  string icon, string title, string description){
            this.Location = Location;
            this.icon = icon;
            this.title = title;
            this.description = description;
            
        }
    }
}