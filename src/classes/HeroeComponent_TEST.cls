@isTest
public class HeroeComponent_TEST {
    @testSetup
    static void setup(){
        Comic__c comic = testFactory.generateComic();
        Heroe__c heroe = testFactory.generateHeroe();
    }
    @isTest
    public static void testHeroeComponent(){
        Heroe__c heroe = [SELECT Real_Name__c, Secret__c FROM Heroe__c];

        Test.startTest();

        system.assertequals(heroe.Real_Name__c,'Hide Name');

        heroe.Secret__c = true;
        update heroe;
        

        Test.stopTest();
    }
}