@RestResource(urlMapping='/HeroeApi/*')
global class ApiHeroeServiceWeb {

    @HttpPost
    global static void upsertHeroe(){
        System.debug('JSON Entrada:' + RestContext.request.requestBody.toString());

        ApiHeroeServiceWebModel req = (ApiHeroeServiceWebModel)JSON.deserialize(RestContext.request.requestBody.toString(), ApiHeroeServiceWebModel.class);
        ApiHeroeServiceWebController.creaHeroe(req);
    }
}