public class HeroeComponent {

    public static void hideWorkStatus(List<Heroe__c> lstDatoNew){

        for(Heroe__c heracr : lstDatoNew){ 
            system.debug('Encriptamos --> ');
            if(heracr.Secret__c == true && String.isNotBlank(heracr.Real_Name__c)){
                Blob cryptoKey = Crypto.generateAesKey(128);
                Blob toEncrypt =   EncodingUtil.base64Decode(heracr.Real_Name__c);
                Blob crypto = Crypto.encryptWithManagedIV('AES128', cryptoKey, toEncrypt);
                heracr.Real_Name__c = EncodingUtil.base64Encode(crypto);
                heracr.CryptoKey2__c = EncodingUtil.base64Encode(cryptoKey);
                system.debug('Dato 1: ' +  heracr.Real_Name__c);
                system.debug('Dato 2: ' +  heracr.CryptoKey2__c);

                system.debug('Ocupation Encriptar: ' + heracr);
            }   
        }
    }   
    public static void sendEmail(List<Heroe__c> lstDatoNew){
        String email =  System.Label.CustomEmails;

        for(Heroe__c her : lstDatoNew){
            system.debug('Email Message');
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = email.split(',');
                message.optOutPolicy = 'SEND';
                message.subject = 'Actualización ' + her.Name;
                message.plainTextBody = 'Hemos actualizado el heroe: ' + her.Name + '\n' +
                                        'Puedes acceder a ver estas modificaciones a traves del siguiente Link \n' + 
                                        'https://salestfg30dosa-dev-ed.lightning.force.com/' + her.id;
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                system.debug('Mensajes');
        }
    }
}