public class PowerstatsController {
	public static void controlDeDatos(List<Powerstats__c> lstnew){
        try{
            for(Powerstats__c powers : lstnew){
                if(powers.Power__c < 0    || powers.Power__c > 100 || powers.Speed__c < 0 || powers.Speed__c > 100 || powers.Strength__c < 0 || powers.Strength__c > 100 ||
                    powers.Durability__c < 0  || powers.Durability__c > 100 || powers.Combat__c < 0    || powers.Combat__c > 100 || powers.Intelligence__c < 0    || powers.Intelligence__c > 100){
                        powers.addError('Por favor ingrese numeros del 1 al 100.');
                }
            }
        }
        catch(Exception ex){
            system.debug('Por favor ingrese numeros del 1 al 100. ' + ex);
        }
    }
}