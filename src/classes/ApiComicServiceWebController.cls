public class ApiComicServiceWebController {

    public static void creaComic(ApiComicServiceWebModel rest){
       
       List<Comic__c> listComic = new List<Comic__c>();
       List<TextObject__c> listtextObj = new List<TextObject__c>();

       for(ApiComicServiceWebModel.cls_results results : rest.data.results){
            
            Comic__c comic = new Comic__c();
                comic.Copyright__c = rest.copyright;
                comic.Description__c = results.description;
                comic.Format__c = results.format;
                comic.Name = results.title;
                comic.PageCount__c = results.pageCount;
                comic.Url_Comic__c = String.join(results.urls, ' ');

            listComic.add(comic);
        
            for(ApiComicServiceWebModel.cls_textObjects resultsTextObjetos : results.textObjects){

                TextObject__c textObjec = new TextObject__c();
                    textObjec.extId__c = String.ValueOf(results.id);
                    textObjec.Description__c = resultsTextObjetos.text;
                    textObjec.Language__c =  resultsTextObjetos.language;

            listtextObj.add(textObjec);
            }
            CollectionUrl__c UrlCollect = new CollectionUrl__c();
            // UrlCollect. = reso;

       }
    }
}