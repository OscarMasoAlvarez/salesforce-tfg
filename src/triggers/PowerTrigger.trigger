trigger PowerTrigger on Powerstats__c (before insert,before update, after insert, after update) {
    if(trigger.isBefore){
         if(trigger.isInsert){
                 PowerstatsController.controlDeDatos(trigger.new);
        }
        if(trigger.isUpdate){

        }
    }
    if(trigger.isAfter){
       if(trigger.isInsert){
        }
        if(trigger.isUpdate){
        }
    }
}