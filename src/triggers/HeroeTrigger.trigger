trigger HeroeTrigger on Heroe__c (before insert,before update, after insert, after update) {
    
    if(trigger.isBefore){
         if(trigger.isInsert){
            HeroeComponent.hideWorkStatus(trigger.new);
        }
        if(trigger.isUpdate){
            HeroeComponent.hideWorkStatus(trigger.new);
            // HeroeComponent.sendEmail(trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            // HeroeComponent.sendEmail(trigger.new);
        }
        if(trigger.isUpdate){
        }
    }
}